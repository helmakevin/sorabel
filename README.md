
##Inventory API for TokoIjah

Dependency Used

 - Go Version 1.12.6
 - SQLITE3 for Database
 - GORM for ORM
 - Gin for routing


EndPoints

 - Get Products /getproducts [get]
	 - http://localhost:8888/api/getproducts
	 
 -  Get Stock In Report /getstockin [get]
	 - http://localhost:8888/api/getstockin
	 
 - Get Stock Out Report /getstockout [get]
	 - http://localhost:8888/api/getstockout
	 
 - Get Product Sales Report /productsales [get]
     * http://localhost:8888/api/productsales
 - Get Valuation Report /productvaluation [get]
    *  http://localhost:8888/api/productvaluation
 - Insert or Update Product [post]
    * http://localhost:8888/api/insertproduct
    * MultiPart Body Key
        * sku 
        * buy_price
        * product_name
        * qty
        * kwitansi
 - Transaction [post]
    * http://localhost:8888/api/transaction
    * Multipart Body Key
        * transaction_type
        * sku 
        * buy_price
        * sell_price
        * product_name
        * qty

To run the app: 
> go run main.go 
> port will be on :8080

        




