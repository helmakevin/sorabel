package app

import (
	"bytes"
	"encoding/csv"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

//init DB
func InitDb() *gorm.DB {
	db, err := gorm.Open("sqlite3", "./db/tokoijah.db")
	if err != nil {
		panic(err)
	}
	db.LogMode(true)
	return db
}

//Get All Product
func GetProducts(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var products []Products
	var responseProduct ResponseProduct

	db.Raw("SELECT sku,product_name,stocks FROM products order by stocks ASC").Scan(&products)

	if len(products) == 0 {
		responseProduct = ResponseProduct{Status: 0, Message: "Data not found", Data: products}
	} else {
		responseProduct = ResponseProduct{Status: 1, Message: "Success", Data: products}
	}
	c.JSON(200, responseProduct)
}

//insert update
func InsertProduct(c *gin.Context) {

	status := 1
	message := "Success"
	var rensponseInsertProduct ResponseInsertProduct
	var newstocks int
	var products Products
	var productsArrs []Products
	var stockIns StockIns

	buyPrice, _ := strconv.Atoi(c.PostForm("buy_price"))
	qty, _ := strconv.Atoi(c.PostForm("qty"))
	currentdatetime := time.Now().Format("2006-01-02 15:04:05")
	stockIns = StockIns{Sku: c.PostForm("sku"), BuyPrice: buyPrice, CreatedDate: currentdatetime, Qty: qty, Kwitansi: c.PostForm("kwitansi")}

	db := InitDb()
	tx := db.Begin()

	if err := tx.Create(&stockIns).Error; err != nil {
		tx.Rollback()
		status = 0
		message = "failed to insert data stockIns"
	} else {
		db.Where("sku = ?", c.PostForm("sku")).First(&products).Limit(1).Scan(&productsArrs)
		if len(productsArrs) > 0 {
			for i, element := range productsArrs {
				if i == 0 {
					newstocks = element.Stocks + qty
				}
			}
			if err := tx.Model(&products).Where("sku = ?", c.PostForm("sku")).Update("stocks", newstocks).Error; err != nil {
				tx.Rollback()
				status = 0
				message = "failed to update data products"
			}
		} else {
			products = Products{Sku: c.PostForm("sku"), Stocks: qty, ProductName: c.PostForm("product_name")}
			if err := tx.Create(&products).Error; err != nil {
				tx.Rollback()
				status = 0
				message = "failed to insert data products"
			}
		}
	}
	if status == 1 {
		rensponseInsertProduct = ResponseInsertProduct{Status: status, Message: message, Data: DataInsertProduct{Stocks: newstocks, Sku: c.PostForm("sku"), ProductName: c.PostForm("product_name"), BuyPrice: buyPrice, CreatedDate: currentdatetime}}
	} else {
		rensponseInsertProduct = ResponseInsertProduct{Status: status, Message: message}
	}
	tx.Commit()
	c.JSON(200, rensponseInsertProduct)
}

//insert to transaction table and stock out
func Transaction(c *gin.Context) {
	tType, _ := strconv.Atoi(c.PostForm("transaction_type")) // 1 : sales , 2 : missing products
	status := 1
	message := "Success"
	var responseTransaction ResponseTransaction
	var newstocks int
	var products Products
	var productsArrs []Products
	var stockInsArrs []StockIns
	var stockOuts StockOuts
	var stockIns StockIns
	var note string

	transaction_id := ""
	sellPrice, _ := strconv.Atoi(c.PostForm("sell_price"))
	var buyPrice int
	qtY, _ := strconv.Atoi(c.PostForm("qty"))
	currentdatetime := time.Now().Format("2006-01-02 15:04:05")

	db := InitDb()
	db.Where("sku = ?", c.PostForm("sku")).First(&products).Limit(1).Scan(&productsArrs)
	//checking if product is found
	if len(productsArrs) > 0 {
		tx := db.Begin()
		 //check transaction type
		if tType == 1 {
			transaction_id = generateTransactionID()
			//get data products
			db.Where("sku = ?", c.PostForm("sku")).First(&stockIns).Limit(1).Scan(&stockInsArrs)
			// get the data after transaction
			for i, element := range stockInsArrs {
				if i == 0 {
					buyPrice = element.BuyPrice
				}
			}
			note = "Order " + transaction_id
			transactions := Transactions{Id: transaction_id, BuyPrice: buyPrice, SellPrice: sellPrice, Qty: qtY, Sku: c.PostForm("sku"), CreatedDate: currentdatetime}
			if err := tx.Create(&transactions).Error; err != nil {
				tx.Rollback()
				status = 0
				message = "failed to insert data transaction"
			}
		} else if tType == 2 {
			note = "Item lost"
		}

		//insert data to stockOuts
		stockOuts = StockOuts{Sku: c.PostForm("sku"), CreatedDate: currentdatetime, Qty: qtY, Note: note, TransactionId: transaction_id}
		if err := tx.Create(&stockOuts).Error; err != nil {
			tx.Rollback()
			status = 0
			message = "failed to insert data stocks outs"
		}

		// get the data stock after transaction
		for i, element := range productsArrs {
			if i == 0 {
				newstocks = element.Stocks - qtY
			}
		}

		//update product stocks in table products
		if err := tx.Model(&products).Where("sku = ?", c.PostForm("sku")).Update("stocks", newstocks).Error; err != nil {
			tx.Rollback()
			status = 0
			message = "failed to update data products"
		}
		tx.Commit()
	} else {
		status = 0
		message = "SKU Not found!"
	}
	if status == 1 {
		responseTransaction = ResponseTransaction{Status: status, Message: message, Data: DataTransaction{Sku: c.PostForm("sku"), BuyPrice: buyPrice, SellPrice: sellPrice, CreatedDate: currentdatetime, ProductName: c.PostForm("product_name"), Stocks: newstocks, TransactionId: transaction_id}}
	} else {
		responseTransaction = ResponseTransaction{Status: status, Message: message}
	}
	defer db.Close()
	c.JSON(200, responseTransaction)
}

//product valuation
func GetProductValuation(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var productvaluation []ProductValuationCsv
	db.Raw("SELECT p.sku AS sku,p.product_name,p.stocks AS qty,avg(si.buy_price) AS avg_buy_price, (avg(si.buy_price)*p.stocks) AS total   FROM products AS p LEFT JOIN stock_ins AS si ON p.sku = si.sku GROUP BY p.sku").Scan(&productvaluation)

	var record [][]string
	record = append(record, []string{"SKU", "Nama Item", "Jumlah", "Rata-Rata Harga Beli", "Total"})

	b := &bytes.Buffer{}
	wr := csv.NewWriter(b)

	for _, element := range productvaluation {
		record = append(record, []string{element.Sku, element.ProductName, element.Qty, element.AvgBuyPrice, element.Total})
	}
	wr.WriteAll(record)

	wr.Flush()

	c.Header("Content-Type", "text/csv")
	c.Header("Content-Disposition", "attachment; filename=productvaluation.csv")
	c.Data(http.StatusOK, "text/csv", b.Bytes())
}

//product sales
func GetProductSales(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var transactions []TransactionsCsv
	db.Raw("SELECT t.id AS id, t.created_date AS created_date, t.sku AS sku,p.product_name AS product_name,t.qty AS qty, t.sell_price AS sell_price, (t.sell_price*t.qty) AS total, t.buy_price AS buy_price, ((t.sell_price*t.qty) - (t.buy_price*t.qty)) AS laba FROM transactions AS t LEFT JOIN products AS p ON t.sku = p.sku").Scan(&transactions)

	var record [][]string
	record = append(record, []string{"ID Pesanan", "Waktu", "SKU", "Nama Barang", "Jumlah", "Harga Jual", "Total", "Harga Beli", "Laba"})

	b := &bytes.Buffer{}
	wr := csv.NewWriter(b)

	for _, element := range transactions {
		record = append(record, []string{element.Id, element.CreatedDate, element.Sku, element.ProductName, element.Qty, element.SellPrice, element.Total, element.BuyPrice, element.Laba})
	}
	wr.WriteAll(record)

	wr.Flush()

	c.Header("Content-Type", "text/csv")
	c.Header("Content-Disposition", "attachment; filename=laporan_penjualan.csv")
	c.Data(http.StatusOK, "text/csv", b.Bytes())

}

//stock out
func GetStockOut(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var stock_outs []StockOutsCsv
	db.Raw("SELECT transaction_id,sku,qty,note,created_date FROM stock_outs order by created_date desc").Scan(&stock_outs)

	var record [][]string
	record = append(record, []string{"ID Transaksi", "SKU", "Jumlah", "Catatan", "Waktu"})

	b := &bytes.Buffer{}
	wr := csv.NewWriter(b)
	for _, element := range stock_outs {
		record = append(record, []string{element.Transaction_Id, element.Sku, element.Qty, element.Note, element.Created_Date})
	}

	wr.WriteAll(record)

	wr.Flush()

	c.Header("Content-Type", "text/csv")
	c.Header("Content-Disposition", "attachment; filename=stock_outs.csv")
	c.Data(http.StatusOK, "text/csv", b.Bytes())
}

//stock in
func GetStockIn(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var stock_ins []StockInsCsv
	db.Raw("SELECT sku,buy_price,qty,kwitansi,created_date FROM stock_ins order by created_date desc").Scan(&stock_ins)

	var record [][]string
	record = append(record, []string{"SKU", "Waktu", "Harga Beli", "Jumlah", "Kwitansi"})

	b := &bytes.Buffer{}
	wr := csv.NewWriter(b)

	for _, element := range stock_ins {
		record = append(record, []string{element.Sku, element.CreatedDate, element.BuyPrice, element.Qty, element.Kwitansi})
	}

	wr.WriteAll(record)

	wr.Flush()

	c.Header("Content-Type", "text/csv")
	c.Header("Content-Disposition", "attachment; filename=stock_ins.csv")
	c.Data(http.StatusOK, "text/csv", b.Bytes())

}

//trans id
func generateTransactionID() string {

	word1 := "ID"
	word2 := string(time.Now().Format("20060102"))

	word3 := rand.Intn(999999 - 100000)

	return word1 + "-" + word2 + "-" + strconv.Itoa(word3)

}
