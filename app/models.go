package app

//Products struct
type Products struct {
	Sku         string `gorm:"not null" form:"sku" json:"sku"`
	ProductName string `gorm:"not null" form:"product_name" json:"product_name"`
	Stocks      int    `gorm:"not null" form:"stocks" json:"stocks"`
}

//StockIns
type StockIns struct {
	Sku         string `gorm:"not null" form:"sku" json:"sku"`
	CreatedDate string `gorm:"not null" form:"created_date" json:"created_date"`
	BuyPrice    int    `gorm:"not null" form:"buy_price" json:"buy_price"`
	Qty         int    `gorm:"not null" form:"qty" json:"qty"`
	Kwitansi    string `form:"kwitansi" json:"kwitansi"`
}

//StockOuts
type StockOuts struct {
	TransactionId string `gorm:"null" form"transaction_id" json:"transaction_id"`
	Sku           string `gorm:"not null" form:"sku" json:"sku"`
	Qty           int    `gorm:"not null" form:"qty" json:"qty"`
	Note          string `gorm:"not null" form:"note" json:"note"`
	CreatedDate   string `gorm:"not null" form:"created_date" json:"created_date"`
}

//StockInsCsv
type StockInsCsv struct {
	Sku         string `csv:"sku"`
	CreatedDate string `csv:"created_date"`
	BuyPrice    string `csv:"buy_price"`
	Qty         string `csv:"qty"`
	Kwitansi    string `csv:"kwitansi"`
}

//StockOutsCsv
type StockOutsCsv struct {
	Transaction_Id string `csv:"transaction_id"`
	Sku            string `csv:"sku"`
	Qty            string `csv:"qty"`
	Note           string `csv:"note"`
	Created_Date   string `csv:"created_date"`
}

//transactions
type Transactions struct {
	Id          string `gorm:"null" form"id" json:"id"`
	Sku         string `gorm:"not null" form:"sku" json:"sku"`
	Qty         int    `gorm:"not null" form:"qty" json:"qty"`
	BuyPrice    int    `gorm:"not null" form:"buy_price" json:"buy_price"`
	SellPrice   int    `gorm:"not null" form:"sell_price" json:"sell_price"`
	CreatedDate string `gorm:"not null" form:"created_date" json:"created_date"`
}

//Transaction CSV
type TransactionsCsv struct {
	Id          string `csv:"id"`
	CreatedDate string `csv:"created_date"`
	Sku         string `csv:"sku"`
	ProductName string `csv:"product_name"`
	Qty         string `csv:"qty"`
	BuyPrice    string `csv:"buy_price"`
	Total       string `csv:"total"`
	SellPrice   string `csv:"sell_price"`
	Laba        string `csv:"laba"`
}

//Get Product valuation CSV
type ProductValuationCsv struct {
	Sku         string `csv:"sku"`
	ProductName string `csv:"product_name"`
	Qty         string `csv:"qty"`
	AvgBuyPrice string `csv:"avg_buy_price"`
	Total       string `csv:"total"`
}

//ResponseProduct Struct
type ResponseProduct struct {
	Status  int        `form:"status" json:"status"`
	Message string     `form:"message" json:"message"`
	Data    []Products `form:"data" json:"data"`
}

//Response Insert Product
type ResponseInsertProduct struct {
	Status  int    `form:"status" json:"status"`
	Message string `form:"message" json:"message"`
	Data    DataInsertProduct
}

//data insert product
type DataInsertProduct struct {
	Sku         string `gorm:"not null" form:"sku" json:"sku"`
	ProductName string `gorm:"not null" form:"product_name" json:"product_name"`
	Stocks      int    `gorm:"not null" form:"stocks" json:"stocks"`
	BuyPrice    int    `gorm:"not null" form:"buy_price" json:"buy_price"`
	CreatedDate string `gorm:"not null" form:"created_date" json:"created_date"`
}

//ResponseTransaction
type ResponseTransaction struct {
	Status  int    `form:"status" json:"status"`
	Message string `form:"message" json:"message"`
	Data    DataTransaction
}

//Data Transaction
type DataTransaction struct {
	TransactionId string `form:"transaction_id" json:"transaction_id"`
	Sku           string `gorm:"not null" form:"sku" json:"sku"`
	ProductName   string `gorm:"not null" form:"product_name" json:"product_name"`
	Stocks        int    `gorm:"not null" form:"stocks" json:"stocks"`
	BuyPrice      int    `gorm:"not null" form:"buy_price" json:"buy_price"`
	SellPrice     int    `gorm:"not null" form:"sell_price" json:"sell_price"`
	CreatedDate   string `gorm:"not null" form:"created_date" json:"created_date"`
}
