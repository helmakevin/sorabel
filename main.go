package main

import (
	"github.com/gin-gonic/gin"
	a "sorabel/app"
)

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}
func main() {

	e := gin.Default()

	e.Use(Cors())

	v := e.Group("api")
	{
		v.GET("/getproducts", a.GetProducts)
		v.POST("/insertproduct", a.InsertProduct)
		v.POST("/transaction", a.Transaction)
		v.GET("/getproductvaluation", a.GetProductValuation)
		v.GET("/getproductsales", a.GetProductSales)
		v.GET("/getstockout", a.GetStockOut)
		v.GET("/getstockin", a.GetStockIn)
	}
	e.Run(":8080")
}

